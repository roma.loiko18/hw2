require('dotenv').config();

const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const Note = require('./note');

const userSchema = new mongoose.Schema({
            username: {
                type: String,
                required: true,
                trim: true,
            },
            password: {
                type: String,
                required: true,
                trim: true,
            },
            tokens: [{
                token: {
                    type: String,
                    required: true,
                },
            }],
            createdDate: {
                type: Date,
                default:
                    Date.now(),
            },
        }
    )
;

userSchema.virtual('notes', {
    ref: 'Note',
    localField: '_id',
    foreignField: 'userId',
});

userSchema.methods.generateAuthToken = async function () {
    const token = jwt.sign({_id: this._id.toString()}, process.env.SECRET);
    this.tokens = this.tokens.concat({token});
    await this.save();
    return token;
};

userSchema.statics.findByCredentials = async (username, password) => {
    const user = await User.findOne({username: username});
    if (!user) {
        throw new Error('Unable to login! (no such user with such username)');
    }

    const isMatch = await bcrypt.compare(password, user?.password);

    if (!isMatch) {
        throw new Error('Unable to login!');
    }

    return user;
};

userSchema.pre('save', async function (next) {
    if (this.isModified('password')) {
        this.password = await bcrypt.hash(this.password, 8);
    }
    next();
});

userSchema.methods.toJSON = function () {
    const user = this.toObject();

    delete user.password;
    delete user.tokens;

    return user;
};


userSchema.pre('remove', async function (next) {
    await Note.deleteMany({userId: this._id});
    next();
});


const User = mongoose.model('User', userSchema);


module.exports = User;
