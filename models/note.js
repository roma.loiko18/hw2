const mongoose = require('mongoose');


const noteSchema = new mongoose.Schema({
    text: {
        type: String,
        required: true,
        trim: true,
        default: 'text',
    },
    completed: {
        type: Boolean,
        required: false,
        default: false,
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User',
    },
    createdDate: {
        type: Date,
        default:
            Date.now(),
    },
});

const Note = mongoose.model('Note', noteSchema);


module.exports = Note;
