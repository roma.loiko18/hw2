const express = require('express');
const auth = require('../middleware/auth');
const Note = require('../models/note');
const {message, messages, logRequest} = require('../utils/message');
const router = new express.Router();


router.post('/api/notes', auth, (req, res) => {
    logRequest(req);

    const note = new Note({
        ...req.body,
        userId: req.user._id,
    });
    try {
        note.save();
        res.status(200).send(message(messages.SUCCESS));
    } catch (e) {
        res.status(500).send(message(messages.SERVER_ERROR));
    }
});

router.get('/api/notes', auth, async (req, res) => {
    logRequest(req);

    const match = {};
    const sort = {};

    const skip = req.query.skip || 0;
    const limit = req.query.limit || 0;
    const offset = req.query.offset || 0;

    if (req.query.completed) {
        match.completed = req.query.completed === 'true';
    }

    if (req.query.sortBy) {
        const parts = req.query.sortBy.split(':');
        sort[parts[0]] = parts[1] === 'desc' ? -1 : 1;
    }

    try {
        await req.user.populate({
            path: 'notes',
            match,
            options: {
                limit: parseInt(limit),
                skip: parseInt(skip),
                offset: parseInt(offset),
                sort: {
                    createdAt: -1,
                },
            },
        }).execPopulate();
        res.status(200).send({offset, limit, skip, notes:req.user.notes});
    } catch (e) {
        res.status(400).send(message(messages.SERVER_ERROR));
    }
});

router.get('/api/notes/:id', auth, async (req, res) => {
    logRequest(req);
    const _id = req.params.id;

    if(!_id){
        return res.status(400).send(message(messages.BAD_REQUEST))
    }

    try {
        const note = await Note.findOne({_id, userId: req.user._id});
        if (!note) {
            return res.status(400).send(message(messages.NO_SUCH_NOTE));
        }
        res.status(200).send({note});
    } catch (e) {
        res.status(500).send(message(messages.SERVER_ERROR));
    }
});

router.delete('/api/notes/:id', auth, async (req, res) => {
    logRequest(req);

    const _id = req.params.id;

    if(!_id){
        return res.status(400).send(message(messages.BAD_REQUEST))
    }

    try {
        const note = await Note.findOneAndDelete({_id, userId: req.user._id});
        if (!note) {
            res.status(400).send(message(messages.NO_SUCH_NOTE));
        }
        res.status(200).send(message(messages.SUCCESS));
    } catch (e) {
        res.status(500).send(message(messages.SERVER_ERROR));
    }
});

router.patch('/api/notes/:id', auth, async (req, res) => {
    const _id = req.params.id;

    if(!_id){
        return res.status(400).send(message(messages.BAD_REQUEST))
    }
    try {
        const note = await Note.findOne({_id, userId: req.user._id});
        if (!note) {
            return res.status(400).send(message(messages.NO_SUCH_NOTE));
        }
        note.completed = !note.completed;
        await note.save();
        res.status(200).send(message(messages.SUCCESS));
    } catch (e) {
        res.status(500).send(message(messages.SERVER_ERROR));
    }
});

router.put('/api/notes/:id', auth, async (req, res) => {
    logRequest(req);

    if(!req.params.id) {
            return res.status(400).send(message(messages.BAD_REQUEST))
    }

    const updates = Object.keys(req.body);
    const availableUpdates = ['text', 'completed'];
    const isUpdated = updates.every(
        (update) => availableUpdates.includes(update),
    );

    if (!isUpdated) {
        return res.status(400).send(message(messages.INVALID_OPTIONS));
    }

    try {
        const note = await Note.findOne({_id: req.params.id, userId: req.user._id});
        if (!note) {
            return res.status(400).send(message(messages.INVALID_PARAMS));
        }

        updates.forEach((update) => note[update] = req.body[update]);
        await note.save();
        res.status(200).send(message(messages.SUCCESS));
    } catch (e) {
        return res.status(500).send(message(messages.SERVER_ERROR));
    }
});

module.exports = router;
