const User = require('../models/user');
const express = require('express');
const userService = require('../service/userService');
const auth = require('../middleware/auth');
const {message, messages, logRequest} = require('../utils/message');


const router = new express.Router();


router.post('/api/auth/register', async (req, res) => {
  logRequest(req);
  const existingUser = await User.findOne({username: req.body.username});

  if (existingUser) {
    return res.status(400).send(message(messages.USER_EXISTS));
  }

  if (!userService.isValidAuthBody(req.body)) {
    return res.status(400).send(message(messages.INVALID_PARAMS));
  }

  try {
    const user = new User(req.body);
    await user.generateAuthToken();
    res.status(200).send(message(messages.SUCCESS));
  } catch (e) {
    res.status(500).send(message(messages.SERVER_ERROR));
  }
});

router.post('/api/auth/login', async (req, res) => {
  logRequest(req);

  if (!userService.isValidAuthBody(req.body)) {
    return res.status(400).send(message(messages.INVALID_PARAMS));
  }

  const user = await User
      .findByCredentials(req.body.username, req.body.password);
  if(!user){
    return res.status(400).send(message(messages.NO_SUCH_USER));
  }

  try {
    const token = await user?.generateAuthToken();
    res.status(200).send({message: messages.SUCCESS, jwt_token: token});
  } catch (error) {
    res.status(500).send(message(messages.SERVER_ERROR));
  }
});


router.get('/api/users/me', auth, async (req, res) => {
  logRequest(req);
  res.status(200).send({user: req.user});
});

router.patch('/api/users/me', auth, async (req, res) => {
  logRequest(req);
  const isValid = await userService
      .comparePasswords(req.body.oldPassword, req.user.password);
  const newPassword = req.body.newPassword;
  if (!isValid) {
    return res.status(400).send(message(messages.PASSWORDS_DO_NOT_MATCH));
  }
  if (!newPassword) {
    return res.status(400).send(message(messages.NO_NEW_PASSWORD_OCCURS));
  }

  try {
    req.user.password = await userService.generateHash(newPassword);
    await req.user.save();
    res.send(message(messages.success));
  } catch (error) {
    res.status(500).send(message(messages.SERVER_ERROR));
  }
});

router.post('/api/users/logout', auth, async (req, res) => {
  try {
    req.user.tokens = req.user.tokens.filter(
        (token) => token.token !== req.token,
    );
    await req.user.save();
    res.send(message(messages.SUCCESS));
  } catch (error) {
    res.status(500).send();
  }
});

router.delete('/api/users/me', auth, async (req, res) => {
  try {
    await req.user.remove();
    res.send(message(messages.SUCCESS));
  } catch (error) {
    res.status(500).send(message(messages.SERVER_ERROR));
  }
});

module.exports = router;


