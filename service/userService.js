const bcrypt = require('bcryptjs');

/**
 * A class service for users
 */
class UserService {
  /**
     * Generates hash.
     * @param {Object} requestBody .
     * @return {boolean}.
     */
  isValidAuthBody(requestBody) {
    const {username, password} = requestBody;
    return !!(username && password);
  }

  /**
     * .
     * @param {string} password .
     * @param {string} hash .
     * @return {Promise<any>}.
     */
  async comparePasswords(password, hash) {
    return await bcrypt.compare(password, hash);
  }

  /**
     * Generates hash.
     * @param {string} password .
     * @return {Promise<any>}.
     */
  async generateHash(password) {
    return await bcrypt.hash(password, 8);
  }
}

module.exports = new UserService();
