require('./db/mongoose');
require('dotenv').config();
const cors = require('cors')

const express = require('express');
const userRouter = require('./routers/user');
const notesRouter = require('./routers/notes');


const app = express();
const port = process.env.PORT || 8080;


app.use(express.json());
app.use(cors());

app.use(userRouter);
app.use(notesRouter);

app.listen(port, () => {
  console.log(`Server is up on port: ${port}`);
});

